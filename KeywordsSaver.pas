unit KeywordsSaver;

interface
uses
  System.JSON,
  System.IOUtils,
  System.Classes,
  System.SysUtils,
  Vcl.Dialogs,
  Vcl.Graphics,
  KeywordsDict;

type
{
   For the moment the TKeywordsSaver class is the only saver. It saves and
   restores the model to a json file. If suddenly we want to add other types
   of savers, then we need to create an interface IKeywordsSaver and implement
   its contract in all savers.
}
  TKeywordsSaver = class(TComponent)
  private
    FFilename: string;
    FEncoding: boolean;
  public
    constructor Create(AOwner: TComponent; Filename: string; Encoding: boolean = false);

    function loadData(Keywords: TKeywordsDictionary; Data: string): boolean;
    function load(Keywords: TKeywordsDictionary): boolean;

    function saveData(Keywords: TKeywordsDictionary; var Data: string): boolean;
    function save(Keywords: TKeywordsDictionary): boolean;
  end;

implementation
uses
  main;

{ TKeywordsSaver }

constructor TKeywordsSaver.Create(AOwner: TComponent; Filename: string; Encoding: boolean = false);
begin
  inherited Create(AOwner);
  FFilename := Filename;
  FEncoding := Encoding;
end;

function TKeywordsSaver.load(Keywords: TKeywordsDictionary): boolean;
begin
  result := false;
  try
    if TFile.Exists(FFilename) then
      loadData(Keywords, TFile.ReadAllText(FFilename, TEncoding.UTF8));
    result := true;
  except
    ShowMessage('������ ������ ����� ' + FFilename);
  end;
end;


function TKeywordsSaver.loadData(Keywords: TKeywordsDictionary; Data: string): boolean;
var
  LListArr: TJSONArray;
  LListObj: TJSONObject;
  LListName: string;
begin
  result := false;
  LListArr := nil;
  Keywords.Clear;
  try
    try
      LListArr := TJSONArray.ParseJSONValue(Data) as TJSONArray;
      for var i:=0 to LListArr.Count-1 do
        begin
          LListObj := LListArr.Items[i].AsType<TJSONObject>;
          LListName := LListObj.Values['name'].Value;
          Keywords.AddList(
            LListName,
            LListObj.Values['color'].AsType<integer>
          );
          for var keywordObj in LListObj.Values['keywords'].AsType<TJSONArray> do
            Keywords.AddKeyword(LListName, keywordObj.Value);
        end;
      result := true;
    except
      ShowMessage('������ ������� ����� ' + FFilename);
    end;
  finally
    if LListArr <> nil then
      LListArr.Free;
  end;
end;


function TKeywordsSaver.save(Keywords: TKeywordsDictionary): boolean;
var
  LData: string;
begin
  result := false;
  try
    if saveData(Keywords, LData) then
    begin
      TFile.WriteAllText(FFilename, LData, TEncoding.UTF8);
      result := true;
    end;
  except
    ShowMessage('������ ���������� � ���� ' + FFilename);
  end;
end;


function TKeywordsSaver.saveData(Keywords: TKeywordsDictionary; var Data: string): boolean;
var
  LListArr: TJSONArray;
  LKeywordArr: TJSONArray;
begin
  result := false;
  LListArr := TJSONArray.Create;
  try
    for var list in Keywords.GetLists do
    begin
      LKeywordArr := TJSONArray.Create;
      for var keyword in list.Keywords do
        LKeywordArr.Add(keyword);

      LListArr.Add(
        TJSONObject.Create
          .AddPair('name', list.Name)
          .AddPair('color', integer(list.Color))
          .AddPair('keywords', LKeywordArr)
      );
    end;
    if FEncoding then
      Data := LListArr.ToJSON
    else
      Data := LListArr.ToString;
    result := true;
  finally
    LListArr.Free;
  end;
end;

end.
