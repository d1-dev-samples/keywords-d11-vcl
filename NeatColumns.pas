unit NeatColumns;

interface
uses
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  System.Generics.Collections,
  System.Math,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  Vcl.ComCtrls,
  Vcl.StdCtrls,
  System.ImageList,
  Vcl.ImgList,
  Vcl.ExtCtrls,
  Vcl.Themes,
  Vcl.GraphUtil;

type
  TNeatCellState = (csSelected, csRowHovered, csColumnHovered);
  TNeatCellStates = set of TNeatCellState;

  TNeatColumn = class
  private
    FVisible: boolean;
    FVisibleOnHover: boolean;
    FHint: String;
    FShowHint: boolean;
    FCursor: TCursor;
    FPaddingLeft: integer;
    FPaddingRight: integer;
  protected
    constructor Create;
    function isStateSelectedOrHot(const State: TOwnerDrawState): boolean;
  public
    procedure Draw(StyleService: TCustomStyleServices; Canvas: TCanvas; const Rect: TRect;
          State: TNeatCellStates; const Value: string); virtual; abstract;
    function GetAdjustedRect(const Rect: TRect): TRect; virtual;
    function ModifyValueByClick(var Value: string): boolean; virtual;

    property Visible: boolean read FVisible write FVisible;
    property VisibleOnHover: boolean read FVisibleOnHover write FVisibleOnHover;
    property ShowHint: boolean read FShowHint write FShowHint;
    property Hint: string read FHint write FHint;
    property Cursor: TCursor read FCursor write FCursor;
    property PaddingLeft: integer read FPaddingLeft write FPaddingLeft;
    property PaddingRight: integer read FPaddingRight write FPaddingRight;
  end;


  TNeatColumnText = class(TNeatColumn)
  private
    FIcon: TIcon;
    FFontSize: integer;
    FFontColor: TColor;
    FFrameColor: TColor;
    FFrameVisible: boolean;
    FUnderlineOnHover: boolean;
    FWidthMax: integer;
    FIconPaddingLeft: integer;
    FIconPaddingRight: integer;
  public
    constructor Create(ImageList: TImageList = nil; ImageIndex: integer = 0);
    destructor Destroy; override;
    procedure Draw(StyleService: TCustomStyleServices; Canvas: TCanvas;
          const Rect: TRect; State: TNeatCellStates; const Value: string); override;
    function GetAdjustedRect(const Rect: TRect): TRect; override;
    property FontSize: integer read FFontSize write FFontSize;
    property FontColor: TColor read FFontColor write FFontColor;
    property FrameColor: TColor read FFrameColor write FFrameColor;
    property FrameVisible: boolean read FFrameVisible write FFrameVisible;
    property UnderlineOnHover: boolean read FUnderlineOnHover write FUnderlineOnHover;
    property IconPaddingLeft: integer read FIconPaddingLeft write FIconPaddingLeft;
    property IconPaddingRight: integer read FIconPaddingRight write FIconPaddingRight;
  end;


  TNeatColumnIcon = class(TNeatColumn)
  private
    FImageList: TImageList;
    FDefaultImageIndex: integer;
    FIcon: TIcon;
  public
    constructor Create(ImageList: TImageList; DefaultImageIndex: integer = 0);
    destructor Destroy; override;
    function GetAdjustedRect(const Rect: TRect): TRect; override;
    procedure Draw(StyleService: TCustomStyleServices; Canvas: TCanvas; const Rect: TRect;
          State: TNeatCellStates; const Value: string); override;
  end;


  TNeatColumnCheck = class(TNeatColumn)
  private
    FSize: TSize;
    FTrueAsString: string;
  public
    constructor Create;
    function GetAdjustedRect(const Rect: TRect): TRect; override;
    procedure Draw(StyleService: TCustomStyleServices; Canvas: TCanvas; const Rect: TRect;
          State: TNeatCellStates; const Value: string); override;
    function ModifyValueByClick(var Value: string): boolean; override;
  end;


  TNeatColumnColor = class(TNeatColumn)
  private
    FSize: TSize;
    FBorderColor: TColor;
  public
    constructor Create(Width, Height: integer; BorderColor: TColor);
    function GetAdjustedRect(const Rect: TRect): TRect; override;
    procedure Draw(StyleService: TCustomStyleServices; Canvas: TCanvas; const Rect: TRect;
          State: TNeatCellStates; const Value: string); override;
  end;

implementation

{ TNeatColumn }

function TNeatColumn.ModifyValueByClick(var Value: string): boolean;
begin
  result := false;
end;

constructor TNeatColumn.Create;
begin
  FVisible := true;
  FVisibleOnHover := false;
  FHint := '';
  FShowHint := false;
  FCursor := crDefault;
end;

function TNeatColumn.GetAdjustedRect(const Rect: TRect): TRect;
begin
  result := Rect;
end;

function TNeatColumn.isStateSelectedOrHot(
  const State: TOwnerDrawState): boolean;
begin
  result := [odSelected, odHotLight] * State <> [];
end;


{ TNeatColumnText }

constructor TNeatColumnText.Create(ImageList: TImageList; ImageIndex: integer);//; AFrameVisible: boolean; AFrameColor: TColor);
begin
  inherited Create;
  FPaddingLeft := 5;
  FIconPaddingLeft := 5;
  FFontSize := 10;
  FFontColor := clWindowText;
  FFrameVisible := false;
  FFrameColor := clActiveCaption;
  FUnderlineOnHover := false;

  if ImageList <> nil then
  begin
    FIcon := TIcon.Create;
    ImageList.GetIcon(ImageIndex, FIcon);
  end;
end;

destructor TNeatColumnText.Destroy;
begin
  if FIcon <> nil then
    FIcon.Free;
  inherited;
end;

procedure TNeatColumnText.Draw(StyleService: TCustomStyleServices; Canvas: TCanvas; const Rect: TRect;
      State: TNeatCellStates; const Value: string);
var
  LRect: TRect;
  LWidth: integer;
  LDetails: TThemedElementDetails;
  LTextFormat: TTextFormatFlags;
begin
  LRect := Rect;
  LWidth := 0;

  if FIcon <> nil then
  begin
    inc(LRect.Left, FIconPaddingLeft);
    Canvas.Draw(LRect.Left, LRect.Top + (LRect.Bottom - LRect.Top - FIcon.Height) div 2, FIcon);
    inc(LRect.Left, FIcon.Width);
    LWidth := FIconPaddingLeft + FIcon.Width + FIconPaddingRight;
  end;

  LDetails := StyleService.GetElementDetails(tgCellNormal);
  LTextFormat := TTextFormatFlags(DT_SINGLELINE or DT_VCENTER or DT_LEFT or DT_END_ELLIPSIS);

  inc(LRect.Left, FPaddingLeft);
  dec(LRect.Right, FPaddingRight);

  Canvas.Font.Size := FFontSize;
  Canvas.Font.Style := [];
  if (FUnderlineOnHover) and (csRowHovered in State) and (csColumnHovered in State) then
    Canvas.Font.Style := [fsUnderline];

  StyleService.DrawText(Canvas.Handle, LDetails, Value, LRect, LTextFormat, FFontColor);

  Inc(LWidth, LWidth + FPaddingLeft + Canvas.TextWidth(Value) + FPaddingRight);
  FWidthMax := max(FWidthMax, LWidth);

  if (FFrameVisible) and (csSelected in State) then begin
    Canvas.Brush.Style := bsClear;
    Canvas.Pen.Color := FFrameColor;
    Canvas.Pen.Style := psSolid;
    Canvas.Rectangle(rect);
  end;
end;


function TNeatColumnText.GetAdjustedRect(const Rect: TRect): TRect;
begin
  Result := Rect;
  Result.Right := Result.Left + FWidthMax;
end;


{ TNeatColumnIcon }

constructor TNeatColumnIcon.Create(ImageList: TImageList; DefaultImageIndex: integer = 0);
begin
  inherited Create;
  FImageList := ImageList;
  FDefaultImageIndex := DefaultImageIndex;
  FIcon := TIcon.Create;
end;

destructor TNeatColumnIcon.Destroy;
begin
  FIcon.Free;
end;

procedure TNeatColumnIcon.Draw(StyleService: TCustomStyleServices; Canvas: TCanvas; const Rect: TRect;
      State: TNeatCellStates; const Value: string);
var
  LImageIndex: integer;
  LAdjRect: TRect;
begin
  LImageIndex := StrToIntDef(Value, FDefaultImageIndex);
  if (FImageList <> nil) and (LImageIndex >= 0) and (LImageIndex < FImageList.Count) then begin
    FImageList.GetIcon(LImageIndex, FIcon);

    LAdjRect := GetAdjustedRect(Rect);
    if LAdjRect.Width <= Rect.Width then
      Canvas.Draw(LAdjRect.Left, LAdjRect.Top, FIcon);
  end;
end;

function TNeatColumnIcon.GetAdjustedRect(const Rect: TRect): TRect;
begin
  if FImageList <> nil then begin
    Result.Top := Rect.Top + (Rect.Bottom - Rect.Top - FImageList.Height) div 2;
    Result.Left := Rect.Left + FPaddingLeft + ((Rect.Width - FPaddingLeft - FPaddingRight - FImageList.Width) div 2);
    Result.Width := FImageList.Width;
    Result.Height := FImageList.Height;
  end
  else
    Result := Rect;
end;


{ TNeatColumnCheck }

constructor TNeatColumnCheck.Create;
begin
  inherited Create;
  FSize.cx := GetSystemMetrics(SM_CXMENUCHECK);
  FSize.cy := GetSystemMetrics(SM_CYMENUCHECK);
  FTrueAsString := true.ToString;
end;


procedure TNeatColumnCheck.Draw(StyleService: TCustomStyleServices; Canvas: TCanvas; const Rect: TRect;
      State: TNeatCellStates; const Value: string);
var
  LDetails: TThemedElementDetails;
  LAdjRect: TRect;
begin
  if (Value = FTrueAsString) then
  begin
    if csRowHovered in State then
      LDetails := StyleService.GetElementDetails(tbCheckBoxCheckedHot)
    else
      LDetails := StyleService.GetElementDetails(tbCheckBoxCheckedNormal);
  end
  else
  begin
    if csRowHovered in State then
      LDetails := StyleService.GetElementDetails(tbCheckBoxUncheckedHot)
    else
      LDetails := StyleService.GetElementDetails(tbCheckBoxUncheckedNormal);
  end;

  LAdjRect := GetAdjustedRect(Rect);
  if LAdjRect.Width <= Rect.Width then
    StyleService.DrawElement(Canvas.Handle, LDetails, LAdjRect);
end;


function TNeatColumnCheck.GetAdjustedRect(const Rect: TRect): TRect;
begin
  Result.Top := Rect.Top + ((Rect.Bottom - Rect.Top - FSize.cy) div 2) + 1;
  Result.Bottom := Result.Top + FSize.cy;
  Result.Left := Rect.Left + FPaddingLeft + ((Rect.Width - FPaddingLeft - FPaddingRight - FSize.cx) div 2);
  Result.Right := Result.Left + FSize.cx;
end;


function TNeatColumnCheck.ModifyValueByClick(var Value: string): boolean;
var
  FBoolValue: boolean;
begin
  Value := (Value <> FTrueAsString).ToString;
  result := true;
end;


{ TNeatColumnColor }

constructor TNeatColumnColor.Create(Width, Height: integer;
  BorderColor: TColor);
begin
  inherited Create;
  FSize.cx := Width;
  FSize.cy := Height;
  FBorderColor := BorderColor;
end;

procedure TNeatColumnColor.Draw(StyleService: TCustomStyleServices; Canvas: TCanvas; const Rect: TRect;
      State: TNeatCellStates; const Value: string);
var
  LAdjRect: TRect;
begin
  LAdjRect := GetAdjustedRect(Rect);
  Canvas.Brush.Color := TColor(StrToIntDef(Value, 0));
  Canvas.Brush.Style := bsSolid;
  Canvas.Pen.Color := FBorderColor;
  Canvas.Pen.Style := psSolid;
  if LAdjRect.Width <= Rect.Width then
    Canvas.Rectangle(LAdjRect);
end;

function TNeatColumnColor.GetAdjustedRect(const Rect: TRect): TRect;
begin
  Result.Top := Rect.Top + ((Rect.Bottom - Rect.Top - FSize.cy) div 2) + 1;
  Result.Bottom := Result.Top + FSize.cy;
  Result.Left := Rect.Left + FPaddingLeft + ((Rect.Width - FPaddingLeft - FPaddingRight - FSize.cx) div 2);
  Result.Right := Result.Left + FSize.cx;
end;


// todo: TNeatColumnProgress

// todo: TNeatColumnRadio

// todo: TNeatColumnMultilineText

end.
