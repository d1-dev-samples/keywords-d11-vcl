unit main;

interface

uses
  KeywordsDict,
  KeywordsSaver,
  NeatListView,

  Generics.Collections,
  System.Classes,
  Vcl.Graphics,
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  Vcl.ExtCtrls,
  Vcl.ComCtrls,
  System.ImageList,
  Vcl.ImgList,
  Vcl.StdCtrls,
  Data.Bind.EngExt,
  Vcl.Bind.DBEngExt,
  System.Rtti,
  System.Bindings.Outputs,
  Vcl.Bind.Editors,
  Data.Bind.Components,
  Vcl.ToolWin,
  Vcl.Buttons,
  Vcl.Imaging.pngimage;

const
  UM_SET_LIST_COLOR_MSG = WM_APP + 1;

type
  TForm1 = class(TForm)
    pnlMasOuter: TPanel;
    Splitter1: TSplitter;
    pnlDetOuter: TPanel;
    pnlMaster: TPanel;
    pnlDetails: TPanel;
    Panel5: TPanel;
    pnlFooter: TPanel;
    pnlMasStub: TPanel;
    pnlMasToolbar: TPanel;
    ImageList: TImageList;
    pnlDetStub: TPanel;
    pnlDetToolbar: TPanel;
    btDefault: TButton;
    btClose: TButton;
    cbMasAll: TCheckBox;
    lbMasHeader: TLabel;
    imMasSort: TImage;
    Panel2: TPanel;
    imMasImport: TImage;
    imMasExport: TImage;
    lbDetHeader: TLabel;
    imDetSort: TImage;
    procedure FormCreate(Sender: TObject);
    procedure MasterSelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btDefaultClick(Sender: TObject);
    procedure btCloseClick(Sender: TObject);
    procedure cbMasAllMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure imMasImportClick(Sender: TObject);
    procedure imMasExportClick(Sender: TObject);
    procedure imMasSortClick(Sender: TObject);
    procedure imDetSortClick(Sender: TObject);
  private
    FMaster: TNeatListView;
    FDetails: TNeatListView;
    FKeywords: TKeywordsDictionary;
    FSaver: TKeywordsSaver;

    procedure MasterEditFinish(Sender: TObject; Success: boolean; Text: string);
    procedure MasterFooterClick(Sender: TObject);
    procedure AddMasterItem(Text: string);
    procedure MasterItemClick(Sender: TObject; Item: TListItem;
      ColumnIndex: integer; var Processed: boolean);
    procedure AddDetailsItem(Text: string);
    procedure DetailsEditFinish(Sender: TObject; Success: boolean;
      Text: string);
    procedure DetailsFooterClick(Sender: TObject);
    procedure DetailsItemClick(Sender: TObject; Item: TListItem;
      ColumnIndex: integer; var Processed: boolean);
    procedure DeleteMasterItem(Item: TListItem);
    procedure ChangeMasterItemColor(Item: TListItem);
    procedure DeleteDetailsItem(Item: TListItem);
    function GetSelectedListName: string;
    procedure UpdateMasterFromModel;
    procedure UpdateDetailsFromModel;
    procedure UpdateCheckboxMasAll;
    procedure CheckAll;
    procedure UpdateExportButton;
    procedure SortList(List: TNeatListView; Image: TImage);
    procedure CreateMaster;
    procedure CreateDetails;
    procedure OnSetListColor(var Msg: TMessage); message UM_SET_LIST_COLOR_MSG;
  end;

var
  Form1: TForm1;

implementation
uses
  NeatColumns;

{$R *.dfm}

const
  FILENAME_SAVE = 'keywords.json';

  MAS_CHECK = 0;
  MAS_NAME = 1;
  MAS_DELETE = 2;
  MAS_COLOR = 3;


  DET_NAME = 0;
  DET_DELETE = 1;

  IMG_PLUS = 13;
  IMG_SORT_ASC = 15;
  IMG_SORT_DESC = 14;
  IMG_IMPORT = 9;
  IMG_EXPORT = 7;
  IMG_EXPORT_DIS = 6;
  IMG_DELETE = 2;
  IMG_LIST = 10;


procedure TForm1.FormCreate(Sender: TObject);
begin
  ReportMemoryLeaksOnShutdown := True;

  FKeywords := TKeywordsDictionary.Create(self);
  FSaver := TKeywordsSaver.Create(self, FILENAME_SAVE);
  FSaver.load(FKeywords);

  CreateMaster;
  CreateDetails;

  UpdateMasterFromModel;
end;


procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FSaver.save(FKeywords);
end;


procedure TForm1.CreateMaster;
var
  nc: TNeatColumn;
  nct: TNeatColumnText;
begin
  FMaster := TNeatListView.Create(pnlMaster);
  with FMaster do
  begin
    SortColumnIndex := MAS_NAME;
    SetRowHeight(30);
    ListView.ShowColumnHeaders := false;

    nc := TNeatColumnCheck.Create;
    nc.PaddingLeft := 10;
    AddColumn(nc, 32);

    nct := TNeatColumnText.Create(ImageList, IMG_LIST);
    nct.IconPaddingLeft := 4;
    nct.FrameVisible := true;
    AddColumn(nct, 0);

    nc := TNeatColumnIcon.Create(ImageList, IMG_DELETE);
    nc.VisibleOnHover := true;
    nc.ShowHint := true;
    nc.Hint := '�������';
    nc.Cursor := crHandPoint;
    AddColumn(nc, 30);

    nc := TNeatColumnColor.Create(20, 20, clActiveBorder);
    nc.PaddingRight := 8;
    nc.ShowHint := true;
    nc.Hint := '���������� ����';
    nc.Cursor := crHandPoint;
    AddColumn(nc, 40);


    nct := TNeatColumnText.Create(ImageList, IMG_PLUS);
    nct.Cursor := crHandPoint;
    nct.UnderlineOnHover := true;
    nct.FontColor := clBlue;
    nct.IconPaddingLeft := 6;
    nct.PaddingLeft := 4;
    SetFooter(nct, '������� ��� ������ ������');
    AddFooterRow;

    OnItemClick := MasterItemClick;
    OnFooterClick := MasterFooterClick;
    OnEditFinish := MasterEditFinish;
    ListView.OnSelectItem := MasterSelectItem;
  end;
end;


procedure TForm1.CreateDetails;
var
  nc: TNeatColumn;
  nct: TNeatColumnText;
begin
  FDetails := TNeatListView.Create(pnlDetails);
  with FDetails do
  begin
    SetRowHeight(30);
    ListView.ShowColumnHeaders := false;

    nc := TNeatColumnText.Create;
    nc.PaddingLeft := 10;
    AddColumn(nc, 0);

    nc := TNeatColumnIcon.Create(ImageList, IMG_DELETE);
    nc.VisibleOnHover := true;
    nc.ShowHint := true;
    nc.Hint := '�������';
    nc.Cursor := crHandPoint;
    nc.PaddingRight := 5;
    AddColumn(nc, 35);

    nct := TNeatColumnText.Create;
    nct.Cursor := crHandPoint;
    nct.UnderlineOnHover := true;
    nct.FontColor := clBlue;
    nct.PaddingLeft := 10;
    SetFooter(nct, '������� ����� �������� �����');
    AddFooterRow;
    SetFooterVisible(false);

    OnItemClick := DetailsItemClick;
    OnFooterClick := DetailsFooterClick;
    OnEditFinish := DetailsEditFinish;
  end;
end;


{ === Master's stuff === }

procedure TForm1.MasterItemClick(Sender: TObject; Item: TListItem; ColumnIndex: integer; var Processed: boolean);
begin
  if ColumnIndex = MAS_CHECK then begin
    UpdateCheckboxMasAll;
    Processed := true;
  end;

  if ColumnIndex = MAS_DELETE then begin
    DeleteMasterItem(item);
    Processed := true;
  end;

  if ColumnIndex = MAS_COLOR then begin
    PostMessage(Handle, UM_SET_LIST_COLOR_MSG, 0, Item.Index);;
//    ChangeMasterItemColor(item);
    Processed := true;
  end;
end;


procedure TForm1.MasterFooterClick(Sender: TObject);
begin
  FMaster.SetFooterVisible(false);
  FMaster.EditCell(FMaster.ListView.Items.Count-1, MAS_NAME);
end;


procedure TForm1.MasterEditFinish(Sender: TObject; Success: boolean; Text: string);
begin
  if Success then
    AddMasterItem(Text);
  FMaster.SetFooterVisible(true);
end;


procedure TForm1.AddMasterItem(Text: string);
var
  FColor: TColor;
begin
  Text := trim(Text);
  if Text = '' then
    exit;

  if FKeywords.ContainsList(Text) then
  begin
    ShowMessage('������ � ����� ������ ��� ����������!');
    exit;
  end;

  FColor := random($1000000);
  if FKeywords.AddList(text, FColor) then
    with FMaster.AddItem(true, ['', Text, '', integer(FColor).ToString]) do
    begin
      Selected := true;
      Focused := true;
    end;
end;


procedure TForm1.DeleteMasterItem(Item: TListItem);
begin
  FKeywords.RemoveList(FMaster.GetItemValue(Item, MAS_NAME));
  FMaster.ListView.Items.Delete(item.Index);
end;


procedure TForm1.OnSetListColor(var Msg: TMessage);
begin
  ChangeMasterItemColor(FMaster.ListView.Items[Msg.LParam]);
end;


procedure TForm1.ChangeMasterItemColor(Item: TListItem);
begin
  with TColorDialog.Create(nil) do
    try
      Color := StrToIntDef(FMaster.GetItemValue(Item, MAS_COLOR), 0);
      options := [cdFullOpen, cdSolidColor];
      if Execute then
      begin
        FKeywords.SetListColor(FMaster.GetItemValue(Item, MAS_NAME), Color);
        FMaster.SetItemValue(Item, MAS_COLOR, integer(Color).ToString);
      end;
    finally
      Free;
    end;
end;


procedure TForm1.MasterSelectItem(Sender: TObject; Item: TListItem;
  Selected: Boolean);
begin
  if (Selected) and (not FMaster.IsFooterItem(Item)) then
  begin
    UpdateDetailsFromModel;
    FDetails.SetFooterVisible(true);
  end
  else
  begin
    FDetails.ClearWithoutFooter;
    FDetails.SetFooterVisible(false);
  end;
  UpdateExportButton;
end;


function TForm1.GetSelectedListName: string;
begin
  if (FMaster.ListView.Selected <> nil) and (not FMaster.IsFooterItem(FMaster.ListView.Selected)) then
    result := FMaster.GetItemValue(FMaster.ListView.Selected, MAS_NAME)
  else
    result := '';
end;


procedure TForm1.UpdateMasterFromModel;
var
  LList: TArray<string>;
begin
  FMaster.ClearWithoutFooter;
  FMaster.ListView.Items.BeginUpdate;
  var lists := FKeywords.GetLists;
  for var list in lists do
      FMaster.AddItem(false, ['0', list.Name, '', integer(list.Color).ToString]);

  FMaster.ListView.Items.EndUpdate;
  FMaster.ListView.AlphaSort;
end;


procedure TForm1.UpdateCheckboxMasAll;
var
  FAll, FNone, FValue: boolean;
begin
  FAll := true;
  FNone := true;
  for var item in FMaster.ListView.Items do
    if not FMaster.IsFooterItem(item) then
    begin
      boolean.TryToParse(FMaster.GetItemValue(item, MAS_CHECK), FValue);
      FAll := FAll and FValue;
      FNone := FNone and (not FValue);
    end;

  if FAll then
    cbMasAll.Checked := true
  else if FNone then
    cbMasAll.Checked := false
  else
    cbMasAll.State := TCheckBoxState.cbGrayed;
end;


procedure TForm1.CheckAll;
begin
  for var item in FMaster.ListView.Items do
    if not FMaster.IsFooterItem(item) then
      FMaster.SetItemValue(item, MAS_CHECK, cbMasAll.Checked.ToString);
end;


procedure TForm1.cbMasAllMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  CheckAll;
  FMaster.ListView.SetFocus;
end;


procedure TForm1.UpdateExportButton;
begin
  imMasExport.Picture.Bitmap:= nil;
  if GetSelectedListName = '' then
  begin
    ImageList.GetBitmap(IMG_EXPORT_DIS, imMasExport.Picture.Bitmap);
    imMasExport.Cursor := crDefault;
  end
  else
  begin
    ImageList.GetBitmap(IMG_EXPORT, imMasExport.Picture.Bitmap);
    imMasExport.Cursor := crHandPoint;
  end;
end;


{ === Details' stuff === }

procedure TForm1.DetailsItemClick(Sender: TObject; Item: TListItem; ColumnIndex: integer; var Processed: boolean);
begin
  if ColumnIndex = DET_DELETE then begin
    DeleteDetailsItem(item);
    Processed := true;
  end;
end;


procedure TForm1.DetailsFooterClick(Sender: TObject);
begin
  FDetails.SetFooterVisible(false);
  FDetails.EditCell(FDetails.ListView.Items.Count-1, DET_NAME);
end;


procedure TForm1.DetailsEditFinish(Sender: TObject; Success: boolean; Text: string);
begin
  FDetails.SetFooterVisible(true);
  if Success then
    AddDetailsItem(Text);
end;


procedure TForm1.AddDetailsItem(Text: string);
begin
  Text := trim(Text);
  if Text = '' then
    exit;

  if FKeywords.ContainsKeyword(GetSelectedListName, Text) then
  begin
    ShowMessage('� ������� ������ ����� �������� ����� ��� ����������!');
    exit;
  end;

  if FKeywords.AddKeyword(GetSelectedListName, text) then
    with FDetails.AddItem(true, [Text, '']) do
    begin
      Selected := true;
      Focused := true;
    end;
end;


procedure TForm1.DeleteDetailsItem(Item: TListItem);
begin
  FKeywords.RemoveKeyword(GetSelectedListName, Item.Caption);
  FDetails.ListView.Items.Delete(item.Index);
end;


procedure TForm1.UpdateDetailsFromModel;
begin
  FDetails.ClearWithoutFooter;
  var sl := FKeywords.GetKeywords(GetSelectedListName);
  if sl <> nil then
    for var keyword in sl do
      FDetails.AddItem(false, [keyword, '']);

  FDetails.ListView.AlphaSort;
end;


{ === Other controller's stuff === }

procedure TForm1.btCloseClick(Sender: TObject);
begin
  // todo: ask - are you really sure???
  Close;
end;


procedure TForm1.imMasExportClick(Sender: TObject);
begin
  // todo: implement export
  if GetSelectedListName <> '' then
    ShowMessage('����� ����� ������� ������ ' + GetSelectedListName);
end;

procedure TForm1.imMasImportClick(Sender: TObject);
begin
  // todo: implement import
  ShowMessage('����� ����� ������ ������ ������');
end;


procedure TForm1.imDetSortClick(Sender: TObject);
begin
  SortList(FDetails, imDetSort);
end;


procedure TForm1.imMasSortClick(Sender: TObject);
begin
  SortList(FMaster, imMasSort);
end;


procedure TForm1.SortList(List: TNeatListView; Image: TImage);
begin
  Image.Picture.Bitmap:= nil;
  if List.SortDirection = sdAscending then
  begin
    List.SortDirection := sdDescending;
    ImageList.GetBitmap(IMG_SORT_DESC, Image.Picture.Bitmap);
  end
  else
  begin
    List.SortDirection := sdAscending;
    ImageList.GetBitmap(IMG_SORT_ASC, Image.Picture.Bitmap);
  end;
  List.ListView.AlphaSort;
end;


procedure TForm1.btDefaultClick(Sender: TObject);
begin
  // todo: it would be great to use resource file here
  FSaver.loadData(FKeywords,
    '[{"name":"���������","color":15710719,"keywords":["������","���������","�'+
    '���������","������������","��������","�������������"]},{"name":"���������'+
    '������� ������������","color":14448508,"keywords":["��������","����������'+
    '�� ��������������","����� ��������������","������������","���������������'+
    '� ��������������","����������������"]},{"name":"���������","color":435724'+
    '2,"keywords":["��� �����","��������","���������","�����������","������","'+
    '��������","������","������","���������","�����"]},{"name":"Cyber security'+
    '","color":8514685,"keywords":["Botnet","Brute forcing","DDOS","Hacker","M'+
    'alware","Mysql injection","Scammers","Spammer","Trojan","Virus","Worm"]},'+
    '{"name":"����������� � ��������. ���������","color":4308296,"keywords":["'+
    '�������������","������","��������","���������","������","���������� ����"'+
    ',"�����","�������������","�������"]}]'
  );
  UpdateMasterFromModel;
  cbMasAll.Checked := false;
end;

end.
