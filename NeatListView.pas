unit NeatListView;

interface

uses
  NeatColumns,
  Winapi.CommCtrl,
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  System.Generics.Collections,
  System.Math,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  Vcl.ComCtrls,
  Vcl.StdCtrls,
  System.ImageList,
  Vcl.ImgList,
  Vcl.ExtCtrls,
  Vcl.Themes,
  Vcl.GraphUtil;

const
  NEAT_FOOTER_MARKER = #$FFFF;

type
  TSortDirection = (sdAscending, sdDescending);

  TNeatEditFinishEvent = procedure(Sender: TObject; Success: boolean;
    Text: string) of object;
  TNeatItemClick = procedure(Sender: TObject; Item: TListItem;
    ColumnIndex: integer; var Processed: boolean) of object;
  TNeatItemDblClick = procedure(Sender: TObject; Item: TListItem;
    ColumnIndex: integer) of object;

  TNeatListView = class(TComponent)
  strict private
  type
    TListViewEx = class(TListView)
    private
      FColumnAutoSizes: array of boolean;
      procedure RestoreColumnAutoSizes;
      procedure SaveColumnAutoSizes;
      procedure HideToolTip(hwndFrom: HWND);
    protected
      procedure CMHintShow(var Message: TCMHintShow); message CM_HINTSHOW;
      procedure WMNotify(var Msg: TWMNotify); message WM_NOTIFY;
    end;

  private
    FListView: TListViewEx;
    FEdit: TEdit;
    FNeatColumns: TObjectList<TNeatColumn>;
    FRowHeightTuner: TImageList;
    FAfterScrollTimer: TTimer;

    FFooterNeatColumn: TNeatColumn;
    FFooterText: string;

    FSortColumnIndex: integer;
    FSortDirection: TSortDirection;

    FLastMouseX: integer;
    FLastMouseY: integer;

    FHoverItem: TListItem;
    FHoverItemX: integer;
    FHoverItemY: integer;
    FHoverCol: integer;
    FCanHover: boolean;

    FLastDrawRowLeft: integer;
    FLastDrawRowHeight: integer;
    FLastDrawRowWidth: integer;

    FEditFinish: TNeatEditFinishEvent;
    FItemClick: TNeatItemClick;
    FItemDblClick: TNeatItemDblClick;
    FFooterClick: TNotifyEvent;

    procedure ListViewInfoTip(Sender: TObject; Item: TListItem;
      var InfoTip: string);
    procedure ListViewCustomDraw(Sender: TCustomListView; const ARect: TRect;
      var DefaultDraw: boolean);
    procedure ListViewDrawItem(Sender: TCustomListView; Item: TListItem;
      Rect: TRect; State: TOwnerDrawState);
    procedure ListViewMouseEnter(Sender: TObject);
    procedure ListViewMouseLeave(Sender: TObject);
    procedure ListViewMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: integer);
    procedure ExecuteAfterScrollTimer(Sender: TObject);
    procedure UpdateCursorHint;
    procedure ListViewDblClick(Sender: TObject);
    procedure ListViewMouseActivate(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y, HitTest: integer;
      var MouseActivate: TMouseActivate);
    function GetHoveredNeatColumn: TNeatColumn;
    procedure ListViewCompare(Sender: TObject; Item1, Item2: TListItem;
      Data: integer; var Compare: integer);

    procedure EditMouseActivate(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y, HitTest: integer;
      var MouseActivate: TMouseActivate);
    procedure EditKeyPress(Sender: TObject; var Key: Char);
    procedure EditExit(Sender: TObject);
    procedure EditFinish(Success: boolean);
  public
    constructor Create(OwnerParent: TWinControl);
    destructor Destroy; override;

    procedure SetRowHeight(RowHeight: integer);
    function AddColumn(NeatColumn: TNeatColumn; AWidth: integer): TListColumn;

    procedure SetItemValue(Item: TListItem; Col: integer; const Value: string);
    function GetItemValue(Item: TListItem; Col: integer): string;

    procedure DoAutoSize;
    procedure SetFooterVisible(Visible: boolean);
    function GetCellRect(Row, Col: integer; var Rect: TRect): boolean;

    procedure EditCell(Row, Col: integer; const Text: string = '');

    procedure SetFooter(NeatColumn: TNeatColumn; Text: string);
    procedure AddFooterRow;
    function IsFooterItem(Item: TListItem): boolean;

    function AddItem(WithSort: boolean; Values: array of string): TListItem;
    procedure ClearWithoutFooter;

    property SortColumnIndex: integer read FSortColumnIndex
      write FSortColumnIndex;
    property SortDirection: TSortDirection read FSortDirection
      write FSortDirection;
    property NeatColumns: TObjectList<TNeatColumn> read FNeatColumns;
    property OnEditFinish: TNeatEditFinishEvent read FEditFinish
      write FEditFinish;
    property OnItemClick: TNeatItemClick read FItemClick write FItemClick;
    property OnItemDblClick: TNeatItemDblClick read FItemDblClick
      write FItemDblClick;
    property OnFooterClick: TNotifyEvent read FFooterClick write FFooterClick;
    property ListView: TListViewEx read FListView;
    property Edit: TEdit read FEdit;
  end;

implementation

procedure TNeatListView.TListViewEx.WMNotify(var Msg: TWMNotify);
begin
  inherited;
  case Msg.NMHdr^.code of

    // disable column autosize while changing column width
    HDN_ENDTRACK:
      begin
        RestoreColumnAutoSizes;
        Repaint;
      end;
    HDN_BEGINTRACK:
      SaveColumnAutoSizes;

    // intercept listview hint for truncated cells
    TTN_SHOW:
      HideToolTip(Msg.NMHdr^.hwndFrom);
  end;
end;

procedure TNeatListView.TListViewEx.HideToolTip(hwndFrom: HWND);
var
  ti: TOOLINFO;
begin
  ti.cbSize := sizeof(TOOLINFO);
  ti.HWND := Handle;
  ti.uId := 0;
  SendMessage(hwndFrom, TTM_DELTOOL, 0, LPARAM(@ti));
end;

procedure TNeatListView.TListViewEx.SaveColumnAutoSizes;
begin
  SetLength(FColumnAutoSizes, Columns.Count);
  for var i := 0 to length(FColumnAutoSizes) - 1 do
  begin
    FColumnAutoSizes[i] := Columns[i].AutoSize;
    Columns[i].AutoSize := false;
  end;
end;

procedure TNeatListView.TListViewEx.RestoreColumnAutoSizes;
begin
  for var i := 0 to length(FColumnAutoSizes) - 1 do
    Columns[i].AutoSize := FColumnAutoSizes[i];
end;

procedure TNeatListView.TListViewEx.CMHintShow(var Message: TCMHintShow);
begin
  inherited;
  // set hint position to mouse cursor
  Message.HintInfo.HintPos.X := Mouse.CursorPos.X + 16;
  Message.HintInfo.HintPos.Y := Mouse.CursorPos.Y + 16;
end;


constructor TNeatListView.Create(OwnerParent: TWinControl);
begin
  if OwnerParent <> nil then
    OwnerParent.InsertComponent(Self);

  FNeatColumns := TObjectList<TNeatColumn>.Create(true);

  FAfterScrollTimer := TTimer.Create(OwnerParent);
  FAfterScrollTimer.Enabled := false;
  FAfterScrollTimer.OnTimer := ExecuteAfterScrollTimer;
  FAfterScrollTimer.Interval := 125;

  FListView := TListViewEx.Create(OwnerParent);
  FListView.Parent := OwnerParent;
  FListView.Align := alClient;
  FListView.ViewStyle := TViewStyle.vsReport;

  FListView.ShowHint := true;
  FListView.RowSelect := true;
  FListView.ReadOnly := true;
  FListView.HideSelection := true;
  FListView.OwnerDraw := true;
  FListView.DoubleBuffered := true;
  FListView.SortType := stData;

  FListView.OnDrawItem := ListViewDrawItem;
  FListView.OnCustomDraw := ListViewCustomDraw;
  FListView.OnInfoTip := ListViewInfoTip;
  FListView.OnMouseEnter := ListViewMouseEnter;
  FListView.OnMouseLeave := ListViewMouseLeave;
  FListView.OnMouseMove := ListViewMouseMove;
  FListView.OnMouseActivate := ListViewMouseActivate;
  FListView.OnDblClick := ListViewDblClick;
  FListView.OnCompare := ListViewCompare;

  FListView.SmallImages := TImageList.Create(OwnerParent);

  FEdit := TEdit.Create(FListView);
  FEdit.Parent := FListView;
  FEdit.OnMouseActivate := EditMouseActivate;
  FEdit.OnKeyPress := EditKeyPress;
  FEdit.OnExit := EditExit;
  FEdit.Visible := false;
  FEdit.Font.Size := 10;

  FSortColumnIndex := 0;
  FSortDirection := sdAscending;
  FLastMouseX := 0;
  FLastMouseY := 0;
  FHoverItem := nil;
  FHoverItemX := 0;
  FHoverItemY := 0;
  FCanHover := false;
end;

destructor TNeatListView.Destroy;
begin
  FNeatColumns.Free;
  if FFooterNeatColumn <> nil then
    FFooterNeatColumn.Free;
end;

// Delete all rows except the footer row
procedure TNeatListView.ClearWithoutFooter;
begin
  FListView.Items.BeginUpdate;
  while FListView.Items.Count > 1 do
    FListView.Items.Delete(0);
  FListView.Items.EndUpdate;
end;

procedure TNeatListView.AddFooterRow;
begin
  with FListView.Items.Add do
  begin
    Caption := NEAT_FOOTER_MARKER;
    for var i := 0 to FListView.Columns.Count - 2 do
      SubItems.Add(NEAT_FOOTER_MARKER);
  end;
end;

procedure TNeatListView.SetFooter(NeatColumn: TNeatColumn; Text: string);
begin
  if FFooterNeatColumn <> nil then
    FFooterNeatColumn.Free;

  FFooterNeatColumn := NeatColumn;
  FFooterText := Text;
end;

function TNeatListView.AddItem(WithSort: boolean; Values: array of string)
  : TListItem;
begin
  if length(Values) <> FListView.Columns.Count then
    raise EArgumentException.Create('Invalid arguments number');

  result := FListView.Items.Add;
  with result do
  begin
    Caption := Values[0];
    for var i := 1 to length(Values) - 1 do
      SubItems.Add(Values[i]);

    if WithSort then
      FListView.AlphaSort;
  end;
end;

function TNeatListView.AddColumn(NeatColumn: TNeatColumn; AWidth: integer)
  : TListColumn;
begin
  FNeatColumns.Add(NeatColumn);
  result := FListView.Columns.Add();
  if AWidth = 0 then
    result.AutoSize := true
  else
    result.Width := AWidth;
end;

procedure TNeatListView.SetRowHeight(RowHeight: integer);
begin
  if FListView <> nil then
    FListView.SmallImages.Height := RowHeight;
end;

procedure TNeatListView.ListViewCompare(Sender: TObject;
  Item1, Item2: TListItem; Data: integer; var Compare: integer);
begin
  if Item1.Caption = NEAT_FOOTER_MARKER then
    Compare := 1
  else if Item2.Caption = NEAT_FOOTER_MARKER then
    Compare := -1
  else
  begin
    if SortColumnIndex = 0 then
      Compare := CompareStr( AnsiUpperCase(Item1.Caption), AnsiUpperCase(Item2.Caption))
    else if SortColumnIndex < min(Item1.SubItems.Count, Item2.SubItems.Count)
    then
      Compare := CompareStr(AnsiUpperCase(Item1.SubItems[SortColumnIndex - 1]),
        AnsiUpperCase(Item2.SubItems[SortColumnIndex - 1]));

    if FSortDirection = sdDescending then
      Compare := -Compare;
  end;
end;

// Need this handler because when mouse wheel scroll happens
// hovered item should be updated
procedure TNeatListView.ListViewCustomDraw(Sender: TCustomListView;
  const ARect: TRect; var DefaultDraw: boolean);
begin
  if FCanHover then
  begin
    if (FHoverItem <> nil) then
    begin
      var
      pp := FHoverItem.GetPosition;
      if (pp.X <> FHoverItemX) or (pp.Y <> FHoverItemY) then
      begin
        FHoverItem.Update;
        FHoverItem := nil;
        UpdateCursorHint;
      end;
    end;
    FAfterScrollTimer.Enabled := false;
    FAfterScrollTimer.Enabled := true;
  end;
end;

procedure TNeatListView.ListViewMouseEnter(Sender: TObject);
begin
  FCanHover := true;
end;

procedure TNeatListView.ListViewMouseLeave(Sender: TObject);
begin
  FCanHover := false;
  if FHoverItem <> nil then
  begin
    FHoverItem.Update;
    FHoverItem := nil;
  end;
end;

procedure TNeatListView.ListViewMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: integer);
var
  LRect: TRect;
  LPoint: TPoint;
begin
  FLastMouseX := X;
  FLastMouseY := Y;

  // find hovered item
  var
  Item := FListView.GetItemAt(X, Y);
  if (Item <> FHoverItem) and (Item <> nil) then
  begin
    var
    itemPos := Item.GetPosition;
    FHoverItemX := itemPos.X;
    FHoverItemY := itemPos.Y;
  end;

  // find hovered column index
  var
  Col := -1;
  if Item <> nil then
  begin
    LPoint.X := X;
    LPoint.Y := Y;
    LRect.Top := FHoverItemY;
    LRect.Left := FLastDrawRowLeft;
    LRect.Height := FLastDrawRowHeight;

    if IsFooterItem(Item) then
    begin
      if FFooterNeatColumn.Visible then
      begin
        LRect.Width := FLastDrawRowWidth;
        if FFooterNeatColumn.GetAdjustedRect(LRect).Contains(LPoint) then
          Col := 0;
      end
      else
        Item := nil;
    end
    else
    begin
      for var i := 0 to FNeatColumns.Count - 1 do
      begin
        LRect.Width := FListView.Columns[i].Width;
        if FNeatColumns[i].GetAdjustedRect(LRect).Contains(LPoint) then
          Col := i;
        inc(LRect.Left, LRect.Width);
      end;
    end;
  end;

  // if changed do update
  if (FHoverItem <> Item) or (FHoverCol <> Col) then
  begin
    if FHoverItem <> nil then
      FHoverItem.Update;

    if Item <> nil then
      Item.Update;

    FHoverItem := Item;
    FHoverCol := Col;

    UpdateCursorHint;
  end;
end;

procedure TNeatListView.UpdateCursorHint;
var
  LNeatColumn: TNeatColumn;
begin
  if (FHoverItem = nil) or (FHoverCol < 0) then
  begin
    FListView.ShowHint := false;
    FListView.Cursor := crDefault;
    exit;
  end;

  LNeatColumn := GetHoveredNeatColumn;
  if LNeatColumn <> nil then
  begin
    FListView.ShowHint := LNeatColumn.ShowHint;
    FListView.Hint := LNeatColumn.Hint;
    FListView.Cursor := LNeatColumn.Cursor;
  end;
end;

function TNeatListView.GetHoveredNeatColumn: TNeatColumn;
begin
  result := nil;
  if FHoverItem <> nil then
  begin
    if IsFooterItem(FHoverItem) then
      result := FFooterNeatColumn
    else if FHoverCol >= 0 then
      result := FNeatColumns[FHoverCol];
  end;
end;

procedure TNeatListView.EditCell(Row, Col: integer; const Text: string);
var
  LRect: TRect;
begin
  if GetCellRect(FListView.Items.Count - 1, Col, LRect) then
  begin
    FEdit.Text := Text;
    FEdit.Left := LRect.Left;
    FEdit.Top := LRect.Top;
    FEdit.Width := LRect.Width;
    FEdit.Visible := true;
    FEdit.SetFocus;
  end;
end;

procedure TNeatListView.EditKeyPress(Sender: TObject; var Key: Char);
begin
  if (Key = #13) or (Key = #27) then
  begin
    EditFinish(Key = #13);
    Key := #0;
  end;
end;

procedure TNeatListView.EditMouseActivate(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y, HitTest: integer;
  var MouseActivate: TMouseActivate);
begin
  // Supress mouse activity while editing
  MouseActivate := maNoActivate;
end;

procedure TNeatListView.EditExit(Sender: TObject);
begin
  EditFinish(false);
end;

procedure TNeatListView.EditFinish(Success: boolean);
begin
  if FEdit.Visible then
  begin
    if Assigned(FEditFinish) then
      FEditFinish(Self, Success, FEdit.Text);

    FEdit.Visible := false;
    FListView.SetFocus;
  end;
end;

procedure TNeatListView.ExecuteAfterScrollTimer(Sender: TObject);
begin
  if FCanHover then
    ListViewMouseMove(Sender, [], FLastMouseX, FLastMouseY);

  FAfterScrollTimer.Enabled := false;
end;

procedure TNeatListView.SetItemValue(Item: TListItem; Col: integer;
  const Value: string);
begin
  if Col = 0 then
    Item.Caption := Value
  else if (Col > 0) and (Col <= Item.SubItems.Count) then
    Item.SubItems[Col - 1] := Value;
end;

function TNeatListView.GetItemValue(Item: TListItem; Col: integer): string;
begin
  if Col = 0 then
    result := Item.Caption
  else if (Col > 0) and (Col <= Item.SubItems.Count) then
    result := Item.SubItems[Col - 1]
  else
    result := '';
end;

procedure TNeatListView.ListViewDrawItem(Sender: TCustomListView;
  Item: TListItem; Rect: TRect; State: TOwnerDrawState);
var
  LRect: TRect;
  LStyleService: TCustomStyleServices;
  LDetails: TThemedElementDetails;
  LState: TNeatCellStates;
  FSelected, FRowHovered: boolean;
begin
  LStyleService := StyleServices;
  if not LStyleService.Enabled then
    exit;

  FLastDrawRowLeft := Rect.Left;
  FLastDrawRowHeight := Rect.Height;
  FLastDrawRowWidth := Rect.Width;

  // clear row canvas before drawing
  Sender.Canvas.Brush.Style := bsSolid;
  Sender.Canvas.Brush.Color := LStyleService.GetSystemColor(clWindow);
  Sender.Canvas.FillRect(Rect);

  // prepare state flags
  LState := [];
  FSelected := [odSelected, odHotLight] * State <> [];
  FRowHovered := Item = FHoverItem;
  if FSelected then
    Include(LState, csSelected);
  if FRowHovered then
    Include(LState, csRowHovered);

  if IsFooterItem(Item) then
  begin
    // footer row drawing
    if FHoverCol = 0 then
      Include(LState, csColumnHovered);

    if FFooterNeatColumn.Visible then
      FFooterNeatColumn.Draw(LStyleService, Sender.Canvas, Rect, LState, FFooterText);
  end
  else
  begin
    // regular row drawing
    if FSelected then
    begin
      LDetails := LStyleService.GetElementDetails(tgCellSelected);
      LStyleService.DrawElement(Sender.Canvas.Handle, LDetails, Rect);
    end;

    LRect := Rect;
    for var i := 0 to FListView.Columns.Count - 1 do
    begin
      if FHoverCol = i then
        Include(LState, csColumnHovered)
      else
        Exclude(LState, csColumnHovered);

      LRect.Right := LRect.Left + FListView.Columns[i].Width;

      if (FNeatColumns[i].Visible) and ((FRowHovered) or (not FNeatColumns[i].VisibleOnHover)) then
        FNeatColumns[i].Draw(LStyleService, Sender.Canvas, LRect, LState, GetItemValue(Item, i));

      inc(LRect.Left, FListView.Columns[i].Width);
    end;
  end;
end;

function TNeatListView.IsFooterItem(Item: TListItem): boolean;
begin
  result :=
    (Item <> nil) and
    (FFooterNeatColumn <> nil) and
    (Item.Index = FListView.Items.Count - 1);
end;

procedure TNeatListView.ListViewInfoTip(Sender: TObject; Item: TListItem;
  var InfoTip: string);
begin
  InfoTip := FListView.Hint;
end;

procedure TNeatListView.DoAutoSize;
begin
  // hack autosizing
  FListView.Width := 0;
end;

procedure TNeatListView.ListViewDblClick(Sender: TObject);
begin
  if (Assigned(FItemDblClick)) and (FHoverItem <> nil) then
    FItemDblClick(Sender, FHoverItem, FHoverCol);
end;

procedure TNeatListView.ListViewMouseActivate(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y, HitTest: integer;
  var MouseActivate: TMouseActivate);
var
  LProcessed: boolean;
  FValue: string;
begin
  FListView.SetFocus;

  // process only left button click
  if Button <> mbLeft then
    exit;

  // actualizing hovered item
  ListViewMouseMove(Sender, [], X, Y);
  if FHoverItem = nil then
    exit;

  LProcessed := false;

  if IsFooterItem(FHoverItem) then
  begin
    // click on footer row
    LProcessed := true;
    if (FHoverCol = 0) and (Assigned(FFooterClick) and
      (FFooterNeatColumn.Visible)) then
      FFooterClick(Sender);
  end
  else
  begin
    // click on regular rows
    if FHoverCol in [0 .. FNeatColumns.Count - 1] then
    begin
      // invoke column click-modifiers
      FValue := GetItemValue(FHoverItem, FHoverCol);
      if FNeatColumns[FHoverCol].ModifyValueByClick(FValue) then
      begin
        SetItemValue(FHoverItem, FHoverCol, FValue);
        LProcessed := true;
      end;

      // invoke click handler
      if Assigned(FItemClick) then
        FItemClick(Sender, FHoverItem, FHoverCol, LProcessed);
    end;
  end;

  if LProcessed then
    MouseActivate := maNoActivateAndEat;
end;

procedure TNeatListView.SetFooterVisible(Visible: boolean);
begin
  if FFooterNeatColumn <> nil then
  begin
    FFooterNeatColumn.Visible := Visible;
    FListView.Repaint;
  end;
end;

function TNeatListView.GetCellRect(Row, Col: integer; var Rect: TRect): boolean;
var
  pt: TPoint;
begin
  result := false;
  if (Row >= 0) and (Row < FListView.Items.Count) and (Col >= 0) and (Col < FListView.Columns.Count) then
  begin
    pt := FListView.Items[Row].GetPosition;
    pt.X := FLastDrawRowLeft;
    for var i := 0 to Col - 1 do
      inc(pt.X, FListView.Columns[i].Width);
    Rect.TopLeft := pt;
    Rect.Width := FListView.Columns[Col].Width;
    Rect.Height := FLastDrawRowHeight;
    result := true;
  end;
end;

end.
